const helloRoutes = [
    {
        path: '/hello',
        component: () => import('@/views/Hello.vue'),
    }
]

export default helloRoutes 