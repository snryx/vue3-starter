import { createRouter, createWebHistory } from "vue-router"
import helloRoutes from "@/router/modules/hello"

const routes = [
    {
        path: "/",
        redirect: '/hello',
    },
    {
        path: "/:catchAll(.*)",
        component: () => import('@/views/404.vue'),
        meta: {
            layout: 'ErrorLayout'
        }
    },
    ...helloRoutes,
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router