export default {
    state: () => ({ 
        name: null
     }),
    mutations: { 
        SET_NAME(state, payload) {
            state.name = payload
        }
     },
    actions: { 
        setName({commit}, payload) {
            commit('SET_NAME', payload)
        }
     },
    getters: { 
        getName(state) {
            return state.name
        }
     }
}