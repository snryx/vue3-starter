import { createStore } from "vuex";
import hello from "./modules/hello";

const store = createStore({
    modules: {
        hello
    }
})

export default store