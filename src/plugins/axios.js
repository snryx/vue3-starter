import axios from "axios";
import { getToken } from '@/util/auth'


axios.defaults.baseURL = import.meta.env.BASE_URL;
axios.defaults.headers.get['Content-Type'] = 'application/json';
axios.defaults.headers.get['Accept-Language'] = 'tr;q=0.9, en;q=0.8';
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Accept-Language'] = 'tr;q=0.9, en;q=0.8';

// Add a request interceptor
axios.interceptors.request.use(function (config) {
  // Do something before request is sent
  const token = getToken()
  config.headers.Authorization = `Bearer ${token}`;
  return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});

// Add a response interceptor
axios.interceptors.response.use(function (response) {
  // Any status code that lie within the range of 2xx cause this function to trigger
  // Do something with response data
  return response;
}, function (error) {
  // Any status codes that falls outside the range of 2xx cause this function to trigger
  // Do something with response error
  return Promise.reject(error);
});

