class User {
    constructor(data) {
        this.name = data.name
        this.surname = data.surname
        this.birthdate = data.birthdate
    }
    fullname() {
        return `${this.name} ${this.surname}`
    }

    age() {
        const currentTime = new Date()
        const diff = currentTime - this.birthdate
        const age = new Date(diff)

        return Math.abs(age.getUTCFullYear() - 1970)
    }
}